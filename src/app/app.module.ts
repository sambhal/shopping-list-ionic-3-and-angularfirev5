import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { AddShoppingItemPage } from '../pages/add-shopping-item/add-shopping-item';
import { EditShoppingItemPage } from '../pages/edit-shopping-item/edit-shopping-item';

import { AngularFireModule } from 'angularfire2';

import { AngularFireDatabaseModule  } from 'angularfire2/database';

import { FIREBASE_CONFIG } from './firebase.config';
import { ShoppingListService } from '../services/shopping-list/shopping-list.service';


@NgModule({
  declarations: [
    MyApp,
    AddShoppingItemPage,
    EditShoppingItemPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule ,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddShoppingItemPage,
    EditShoppingItemPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ShoppingListService,
  ]
})
export class AppModule {}
