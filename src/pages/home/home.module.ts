import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';


@NgModule({
	declarations:[HomePage],
	imports: [IonicPageModule.forChild(HomePage),CommonModule],
	entryComponents: []
})
export class HomeModule{

}

