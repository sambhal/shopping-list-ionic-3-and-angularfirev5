import { Component } from '@angular/core';

import { NavController, IonicPage } from 'ionic-angular';
import { AddShoppingItemPage } from '../add-shopping-item/add-shopping-item';
import { EditShoppingItemPage } from '../edit-shopping-item/edit-shopping-item';
import { ShoppingListService } from '../../services/shopping-list/shopping-list.service'
import { Observable } from 'rxjs/Observable';
import { Item } from '../../models/item/item.model';

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	shoppingList$: Observable<Item[]>;
	addListPage:any;
	editListPage:any;
	
	constructor(public navCtrl: NavController, private shopping:ShoppingListService) {
		console.log('hi');
		this.addListPage = AddShoppingItemPage;
		this.editListPage = EditShoppingItemPage;

		this.shoppingList$ = this.shopping
		.getShoppingList()
		.snapshotChanges()
		.map(changes => {
			console.log('changes',changes);
			return changes.map(c => ({
				key: c.payload.key, ...c.payload.val()
			}))

		})


	}

}
